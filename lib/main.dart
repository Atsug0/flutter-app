import 'dart:ffi';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/data.dart';
import 'package:flutter_application_1/api_controller.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:lite_rolling_switch/lite_rolling_switch.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    Data data;


    return Scaffold(
        appBar: AppBar(
          title: const Text("List of device"),
        ),
        body: FutureBuilder<Data>(
          future: ApiProvider().callApi(),
          builder: (context, snapshot) {
            data = snapshot.data as Data;
            if (snapshot.hasData) {
              return ListView.builder(
                  itemCount: data.devices.length,
                  itemBuilder: (context, index) {
                    Device? userDevice = data.devices[index];
                    String result =
                        "${userDevice.deviceName} :${userDevice.getState()}";
                    return TextButton(
                        style: ButtonStyle(
                          foregroundColor:
                              MaterialStateProperty.all<Color>(Colors.blue),
                          overlayColor:
                              MaterialStateProperty.resolveWith<Color?>(
                            (Set<MaterialState> states) {
                              if (states.contains(MaterialState.hovered))
                                return Colors.blue.withOpacity(0.04);
                              if (states.contains(MaterialState.focused) ||
                                  states.contains(MaterialState.pressed))
                                return Colors.blue.withOpacity(0.12);
                              return null; // Defer to the widget's default.
                            },
                          ),
                        ),
                        onPressed: () async {
                          Device set = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      ScreenTwo(data: data.devices[index])));
                          if (set.getState() ==
                              data.devices[index].getState()) {
                            Navigator.pushAndRemoveUntil(context,MaterialPageRoute(builder: (context) =>NextPage(data: data)),(Route<dynamic> route) => false);
                          }
                        },
                        child: Text(data.devices[index].deviceName +
                                " :" +
                                data.devices[index].getState()));
                  });
            } else {
              return const CircularProgressIndicator();
            }
          },
        ));
  }
}

class NextPage extends StatefulWidget {
  NextPage({super.key, required this.data});

  Data data;
  @override
  State<NextPage> createState() => _NextPage(data);
}

class _NextPage extends State<NextPage> {
  _NextPage(this.data);

  Data data;
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          title: const Text("List of device"),
        ),
        body: ListView.builder(
            itemCount: data.devices.length,
            itemBuilder: (context, index) {
              Device? userDevice = data.devices[index];
              String result =
                  "${userDevice.deviceName} :${userDevice.getState()}";
              return TextButton(
                  style: ButtonStyle(
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.blue),
                    overlayColor: MaterialStateProperty.resolveWith<Color?>(
                      (Set<MaterialState> states) {
                        if (states.contains(MaterialState.hovered))
                          return Colors.blue.withOpacity(0.04);
                        if (states.contains(MaterialState.focused) ||
                            states.contains(MaterialState.pressed))
                          return Colors.blue.withOpacity(0.12);
                        return null; // Defer to the widget's default.
                      },
                    ),
                  ),
                  onPressed: () async {
                    Device set = await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                ScreenTwo(data: data.devices[index])));
                    if (set.getState() == data.devices[index].getState()) {
                      Navigator.pushAndRemoveUntil(context,MaterialPageRoute(builder: (context) =>NextPage(data: data)),(Route<dynamic> route) => false);
                    }
                  },
                  child:Text(data.devices[index].deviceName +
                          " :" +
                          data.devices[index].getState()));
            }));
  }
}

class ScreenTwo extends StatefulWidget {
  ScreenTwo({super.key, required this.data});

  Device data;
  @override
  State<ScreenTwo> createState() => _ScreenTwo(data);
}

class _ScreenTwo extends State<ScreenTwo> {
  _ScreenTwo(this.data);

  Device data;
  TextEditingController valueTextController = TextEditingController();
  Widget build(BuildContext context) {
    // TODO: implement build
    if (data.productType.toString() == "ProductType.ROLLER_SHUTTER") {
      return Scaffold(
          appBar: AppBar(title: Text(data.deviceName)),
          body: Column(
            children: [
              TextField(
                controller: valueTextController,
              ),
              Slider(
                  value: data.position.toDouble(),
                  min: 0,
                  max: 100,
                  onChanged: ((value) {
                    setState(() {
                      data.position = value.toInt();
                      valueTextController.text = value.toInt().toString();
                      if (value > 0)
                        data.mode = "ON";
                      else
                        data.mode = "OFF";
                    });
                  })),

              TextButton(
                  onPressed: () {
                    Navigator.pop(context, data);
                  },
                  child: Text("Save and go Back"))
            ],
          ));
    }
    if (data.productType.toString() == "ProductType.LIGHT") {
      return Scaffold(
          appBar: AppBar(title: Text(data.deviceName)),
          body: Column(
            children: [
              TextField(
                controller: valueTextController,
              ),
              Slider(
                  value: data.intensity.toDouble(),
                  min: 0,
                  max: 100,
                  onChanged: ((value) {
                    setState(() {
                      data.intensity = value.toInt();
                      valueTextController.text = value.toInt().toString();
                      if (value > 0)
                        data.mode = "ON";
                      else
                        data.mode = "OFF";
                    });
                  })),
                  LiteRollingSwitch(
                value: data.mode == "ON",
                textOn: "On",
                textOff: "Off",
                colorOn: Colors.greenAccent,
                colorOff: Colors.redAccent,
                iconOn: Icons.done,
                iconOff: Icons.light_mode,
                textSize: 18.0,
                onChanged: (bool position) {
                  if (position)
                    data.mode = "ON";
                  else
                    data.mode = "OFF";
                },
              ),
              TextButton(
                  onPressed: () {
                    Navigator.pop(context, data);
                  },
                  child: Text("Save and go Back"))
            ],
          ));
    }
    if (data.productType.toString() == "ProductType.HEATER") {
      return Scaffold(
          body: Column(
            children: [TextButton(
              onPressed: () {
                Navigator.pop(context, data);
              },
              child: Text("Save and go Back"))]));
    }
    return Scaffold();
  }
}