import 'dart:convert';

import 'package:flutter/gestures.dart';
import 'package:flutter_application_1/data.dart';
import 'package:http/http.dart' as http;

class ApiProvider {
  Future<Data> callApi() async {
    var response = await http.get('http://storage42.com/modulotest/data.json');

    var body = response.body;
    Data data = dataFromJson(body);
    return data;
  }
}
