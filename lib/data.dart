// To parse this JSON data, do
//
//     final data = dataFromJson(jsonString);

import 'dart:convert';

Data dataFromJson(String str) => Data.fromJson(json.decode(str));

String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    required this.devices,
    required this.user,
  });

  List<Device> devices;
  User user;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        devices:
            List<Device>.from(json["devices"].map((x) => Device.fromJson(x))),
        user: User.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "devices": List<dynamic>.from(devices.map((x) => x.toJson())),
        "user": user.toJson(),
      };
  List<Device> getDevice() {
    return devices;
  }
}

class Device {
  Device({
    required this.id,
    required this.deviceName,
    required this.intensity,
    required this.mode,
    required this.productType,
    required this.position,
    required this.temperature,
  });

  int id;
  String deviceName;
  int intensity;
  String mode;
  ProductType? productType;
  int position;
  int temperature;

  factory Device.fromJson(Map<String, dynamic> json) => Device(
        id: json["id"],
        deviceName: json["deviceName"],
        intensity: json["intensity"] == null ? null : json["intensity"],
        mode: json["mode"] == null ? null : json["mode"],
        productType: productTypeValues.map[json["productType"]],
        position: json["position"] == null ? null : json["position"],
        temperature: json["temperature"] == null ? null : json["temperature"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "deviceName": deviceName,
        "intensity": intensity == null ? null : intensity,
        "mode": mode == null ? null : mode,
        "productType": productTypeValues.reverse[productType],
        "position": position == null ? null : position,
        "temperature": temperature == null ? null : temperature,
      };
  

  increment() {
    if (intensity != null) {
      intensity = intensity + 1;
    } else {
      if (position != null)
        position = position + 1;
      else {
        if (temperature != null) temperature = temperature + 1;
      }
    }
  }

  void decrementLight() {
    this.intensity--;
  }

  String getState() {
    if (productType.toString() == "ProductType.ROLLER_SHUTTER") {
      if (position == 0) return ("closed");
      if (position == 100) return ("opened");
      return ("opened at " + position.toString() + "%");
    }
    if (productType.toString() == "ProductType.LIGHT") {
      if (intensity == 0 || mode == "OFF") return ("off");
      if (intensity == 100 && mode == "ON") return ("on");
      if (intensity > 0 && mode == "ON")
        return ("on at " + intensity.toString() + "%");
    }
    if (productType.toString() == "ProductType.HEATER") {
      if (mode == "OFF") return ("off");
      if (mode == "ON") return ("on at " + temperature.toString() + "°C");
    }

    return ("");
  }
}

enum ProductType { LIGHT, ROLLER_SHUTTER, HEATER }

final productTypeValues = EnumValues({
  "Heater": ProductType.HEATER,
  "Light": ProductType.LIGHT,
  "RollerShutter": ProductType.ROLLER_SHUTTER
});

class User {
  User({
    required this.firstName,
    required this.lastName,
    required this.address,
    required this.birthDate,
  });

  String firstName;
  String lastName;
  Address address;
  int birthDate;

  factory User.fromJson(Map<String, dynamic> json) => User(
        firstName: json["firstName"],
        lastName: json["lastName"],
        address: Address.fromJson(json["address"]),
        birthDate: json["birthDate"],
      );

  Map<String, dynamic> toJson() => {
        "firstName": firstName,
        "lastName": lastName,
        "address": address.toJson(),
        "birthDate": birthDate,
      };
}

class Address {
  Address({
    required this.city,
    required this.postalCode,
    required this.street,
    required this.streetCode,
    required this.country,
  });

  String city;
  int postalCode;
  String street;
  String streetCode;
  String country;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        city: json["city"],
        postalCode: json["postalCode"],
        street: json["street"],
        streetCode: json["streetCode"],
        country: json["country"],
      );

  Map<String, dynamic> toJson() => {
        "city": city,
        "postalCode": postalCode,
        "street": street,
        "streetCode": streetCode,
        "country": country,
      };
}

class EnumValues<T> {
  Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
